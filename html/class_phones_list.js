var class_phones_list =
[
    [ "PhonesList", "class_phones_list.html#a6b880cbab3a21c8f326ff7441216ae2d", null ],
    [ "~PhonesList", "class_phones_list.html#a95ea0468cefc883b5ea4192014c6e1e5", null ],
    [ "on_addNumberButton_clicked", "class_phones_list.html#ad4c6725d75a983401ef0fdda24b2cb2c", null ],
    [ "on_phonesList_itemClicked", "class_phones_list.html#a44973c5e7d10c0c201358ed04d377452", null ],
    [ "on_searchBar_textChanged", "class_phones_list.html#a2d8819667e98f28b75f899195e07c00d", null ],
    [ "recievePhoneBook", "class_phones_list.html#afb8cc1689234d60d17b12e8f8669d017", null ],
    [ "slotError", "class_phones_list.html#a4d87fdc6467be14d840d1c195d12b758", null ],
    [ "addEditWindow", "class_phones_list.html#a6a494c60a85926d7d05505137067ab93", null ],
    [ "nextBlockSize", "class_phones_list.html#a041a046e04aa8bbc6a0ce733724f79b6", null ],
    [ "phones", "class_phones_list.html#adeb1d103e0014ea930656f1342d9e8db", null ],
    [ "showWindow", "class_phones_list.html#ab61070b252d83642b116649c0f3e5f51", null ],
    [ "socket", "class_phones_list.html#a842fddc845f80b167165302031779c6d", null ],
    [ "ui", "class_phones_list.html#abd8442a5f2b499bee4eb6fa428f27031", null ]
];