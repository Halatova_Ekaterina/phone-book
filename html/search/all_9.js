var searchData=
[
  ['sendphonebooktoclient_24',['sendPhoneBookToClient',['../class_server_window.html#a374855b370cc6af35afb3479ae8f78d5',1,'ServerWindow']]],
  ['server_25',['server',['../class_server_window.html#a77eca29a969602c56d82e1617787ae82',1,'ServerWindow']]],
  ['serverwindow_26',['ServerWindow',['../class_server_window.html',1,'']]],
  ['serverwindow_2eh_27',['serverWindow.h',['../server_window_8h.html',1,'']]],
  ['showwindow_28',['ShowWindow',['../class_show_window.html',1,'ShowWindow'],['../class_phones_list.html#ab61070b252d83642b116649c0f3e5f51',1,'PhonesList::showWindow()']]],
  ['showwindow_2eh_29',['showWindow.h',['../show_window_8h.html',1,'']]],
  ['sloterror_30',['slotError',['../class_phones_list.html#a4d87fdc6467be14d840d1c195d12b758',1,'PhonesList']]],
  ['slotnewconnection_31',['slotNewConnection',['../class_server_window.html#a3670c5d97bebbd4930153a0c7e6e2eef',1,'ServerWindow']]],
  ['socket_32',['socket',['../class_add_edit_window.html#a38749364745e1d666593b12d45ce4420',1,'AddEditWindow::socket()'],['../class_phones_list.html#a842fddc845f80b167165302031779c6d',1,'PhonesList::socket()'],['../class_show_window.html#a4ec91f74fae0b47e0592335cd5e1f594',1,'ShowWindow::socket()']]]
];
