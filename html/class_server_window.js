var class_server_window =
[
    [ "ServerWindow", "class_server_window.html#a17fbfb5945bb3949c23189aeec66635d", null ],
    [ "~ServerWindow", "class_server_window.html#ad93d9d7dbcaa9978a9aa41ed0ee572e5", null ],
    [ "clientReadSlot", "class_server_window.html#abf49f738b7ca3be1181a13934dae6f3c", null ],
    [ "sendPhoneBookToClient", "class_server_window.html#a374855b370cc6af35afb3479ae8f78d5", null ],
    [ "slotNewConnection", "class_server_window.html#a3670c5d97bebbd4930153a0c7e6e2eef", null ],
    [ "tryToAdd", "class_server_window.html#adbc4cf8924ebd32edeb860d6594a4c08", null ],
    [ "tryToDel", "class_server_window.html#a5b4be689dfb16523be9584f038b44eb6", null ],
    [ "tryToEdit", "class_server_window.html#a5171604a8ab509acb44b711b2fbf6b3e", null ],
    [ "nextBlockSize", "class_server_window.html#aae9fd845a998581b3e2f1ea1a21ef143", null ],
    [ "path", "class_server_window.html#aa19e95f5c5d13f06e5ad817dbe499324", null ],
    [ "phones", "class_server_window.html#ac93ca997378e1339da50901be243182b", null ],
    [ "server", "class_server_window.html#a77eca29a969602c56d82e1617787ae82", null ],
    [ "ui", "class_server_window.html#a69f6fae3360979d8f9a5499f933dd8a9", null ]
];